#     __  __    _                    _           _
#   /|  \/  |  (_)                  (_)         (_)
#  | | \  / |   _  __ ____  __   __  _  _ ____   _   _ ____    _____
#  | | |\/| | /| |/ /  _  \/\ \ / //| |/ / __/ /| | / / _  \ /| '_  \
#  | | |  | || | || | (_) |\ \ V /| | |\ \__ \| | || | (_) || | | | |
#  |_|_|  |_||_|_/ \_\___/  \_\_/ |_|_/|_|___/|_|_/ \_\___/ |_|_/ |_/
#
#  Copyright 2022, Miovision
#  @name    metrics.py 
#  @brief   The file contains Functions for Metrics
#  @author  Umer Rasheed

# Import Modules
import numpy as np

# Define a Function to Compute MSE between two Images
def mse(imageA, imageB):
    """
    @brief: Computes MSE between two Images
    @args:
        param <input> First Image
        param <input> Second Image
    """
    mse = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    mse /= float(imageA.shape[0] * imageA.shape[1])
    return mse

# Define a Function to Compute Euclidean Distance between two Arrays
def euclidean_distance(x, y):
    """
    @brief: Computes Euclidean Distance
    @args:
        param <input> Array
        param <input> Array
    """
    return np.sqrt(np.sum(np.square(x - y)))