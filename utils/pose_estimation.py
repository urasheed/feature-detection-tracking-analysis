#     __  __    _                    _           _
#   /|  \/  |  (_)                  (_)         (_)
#  | | \  / |   _  __ ____  __   __  _  _ ____   _   _ ____    _____
#  | | |\/| | /| |/ /  _  \/\ \ / //| |/ / __/ /| | / / _  \ /| '_  \
#  | | |  | || | || | (_) |\ \ V /| | |\ \__ \| | || | (_) || | | | |
#  |_|_|  |_||_|_/ \_\___/  \_\_/ |_|_/|_|___/|_|_/ \_\___/ |_|_/ |_/
#
#  Copyright 2022, Miovision
#  @name    pose_estimation.py
#  @brief   The file contains classes for Pose Estimation
#  @author  Umer Rasheed

# Import Modules
import cv2
import numpy as np
import time
from cv2.xfeatures2d import matchGMS

# Define a Class for Feature Detection
class FeatureDetector:
    # Define Constructor for Feature Detector
    def __init__(self, mode = "Shi-Tomasi", output = "Corners", verbose = True):
        """
        @brief: Constructor for FeatureDetector Class
        @args:
            param <input> Mode (Currently it is either Shi-Tomasi or FAST)
            param <input> Output (Currently either Corners or Keypoints or Both)
            param <input> Flag for Verbose
        """
        # Initialize Variables
        self.mode = mode
        self.output = output
        self.verbose = verbose
        
        # Print Parameters if Flag is set
        if self.verbose:
            print("Feature Extractor:", self.mode, "Output:", self.output)
        
        # Set Variables based on Mode 
        if (self.mode == "Shi-Tomasi"):
            # Set Hyperparameters, Feature Detection
            self.max_num_of_features = 200
            self.feature_quality = 0.001
            self.min_distance_bw_features = 10
        elif (self.mode == "FAST"):
            # Initiate FAST Feature Detector Object with Default Values
            self.fast_detector = cv2.FastFeatureDetector_create()
        
        # Set Variables for Pixel Location Refinement
        self.win_size = (5,5)
        self.zero_zone = (-1,-1)
        self.criteria = (cv2.TERM_CRITERIA_EPS + cv2.TermCriteria_COUNT,40,0.001)
            
    # Define a Functor to Extract Features
    def extract(self, gray):
        """
        @brief: Extracts Features
        @args:
            param <input> Input Image
        """
        # Extract Features if Mode is Shi-Tomasi
        if (self.mode == "Shi-Tomasi"):
            # Apply Shi-Tomasi Corner Detection
            start_time = time.time()
            corners = cv2.goodFeaturesToTrack(gray, maxCorners = self.max_num_of_features, 
                                  qualityLevel = self.feature_quality, 
                                  minDistance = self.min_distance_bw_features)
            end_time = time.time()
            computation_time_corner_detection = 1000 * (end_time - start_time)

            # Print Number of Features and Computation Time if Flag is set
            if self.verbose:
                print("Number of Features", len(corners))
                print("Computation Time for Corner Detection (ms)", computation_time_corner_detection)

            # Return
            if (self.output == "Corners"):
                return corners
            elif (self.output == "Keypoints"):
                return self.convert_to_keypoints(corners)
        
        # Extract Features if Mode is FAST
        elif (self.mode == "FAST"):
            # Apply FAST Corner Detection
            start_time = time.time()
            keypoints = self.fast_detector.detect(gray, None)
            end_time = time.time()
            computation_time_corner_detection = 1000 * (end_time - start_time)

            # Print Number of Features and Computation Time if Flag is set
            if self.verbose:
                print("Number of Features", len(keypoints))
                print("Computation Time for Corner Detection (ms)", computation_time_corner_detection)
            
            # Return
            if (self.output == "Corners"):
                return self.convert_to_corners(keypoints)
            elif (self.output == "Keypoints"):
                return keypoints
     
    # Define a Function to Refine Pixel Locations
    def refine(self, gray, corners):
        """
        @brief: Extracts Features
        @args:
            param <input> Input Image
            param <input> Corners
        """
        # Calculate the Refined Corner Locations
        start_time = time.time()
        corners_refined = cv2.cornerSubPix(gray, corners, 
                                           self.win_size, 
                                           self.zero_zone, 
                                           self.criteria)
        end_time = time.time()
        computation_time_corner_location_refinement = 1000 * (end_time - start_time)

        # Print Computation Time if Flag is set
        if self.verbose:
            print("Computation Time for Corner Location Refinement (ms)", computation_time_corner_location_refinement)
        return corners_refined
    
    # Define a Function to Convert Corners to OpenCV Keypoints
    def convert_to_keypoints(self, corners):
        """
        @brief: Converts Corners to Keypoints
        @args:
            param <input> Corners
        """
        # Convert to Keypoints
        start_time = time.time()
        kps = [cv2.KeyPoint(corner[0][0], corner[0][1], 0) for corner in corners]
        end_time = time.time()
        computation_time_corner_to_keypoints = 1000 * (end_time - start_time)

        # Print Computation Time if Flag is set
        if self.verbose:
            print("Computation Time for Conversion from Corners to Keypoints (ms)", computation_time_corner_to_keypoints)
        return kps
    
    # Define to Function to Convert Keypoints to Corners
    def convert_to_corners(self, keypoints):
        """
        @brief: Converts Keypoints to Corners
        @args:
            param <input> Keypoints
        """
        # Convert to Corners
        start_time = time.time()
        corners = cv2.KeyPoint_convert(keypoints)
        end_time = time.time()
        computation_time_keypoints_to_corners = 1000 * (end_time - start_time)

        # Print Computation Time if Flag is set
        if self.verbose:
            print("Computation Time for Conversion from Keypoints to Corners (ms)", computation_time_keypoints_to_corners)
        return corners
        
# Define a Class for Feature Tracking
class FeatureTracker:
    # Define Constructor for Feature Tracker
    def __init__(self, mode = "LK", verbose = True):
        """
        @brief: Constructor for FeatureTracker Class
        @args:
            param <input> Mode (Currently only Lukar-Kanade Mode is implemented)
            param <input> Flag for Verbose
        """
        # Initialize Variables
        self.mode = mode
        self.verbose = verbose

        # Set Hyperparameters for GMS Filter
        self.scale_flag = False
        self.rotation_flag = False
        self.threshold_factor = 6

    # Define a Function to Track Features
    def track(self, gray, curr_gray, corners):
        """
        @brief: Tracks Features
        @args:
            param <input> Input Image
            param <input> Current Image
        """
        # Track Features with Lukas-Kanade
        if (self.mode == "LK"):
            # Compute Optical Flow
            start_time = time.time()
            curr_corners, status, err = cv2.calcOpticalFlowPyrLK(gray, curr_gray, corners, None)
            end_time = time.time()
            computation_time_tracking = 1000 * (end_time - start_time)
            
            # Select Tracked Corners
            curr_corners = curr_corners[status==1]
            corners = corners[status==1]

            # Print Number of Features Tracked Computation Time if Flag is set
            if self.verbose:
                print("Computation Time for Tracking (ms)", computation_time_tracking)
                print("Number of Features Tracked", len(corners))

            return corners, curr_corners

    # Define a Function to Get Matches
    def convert_to_matches(self, corners):
        """
        @brief: Converts Tracked Corners to Matches
        @args:
            param <input> Tracked Corners
        """
        # Convert to Matches
        start_time = time.time()
        matches = [cv2.DMatch(i,i,0) for i in range(len(corners))]
        end_time = time.time()
        computation_time_get_matches = 1000 * (end_time - start_time)

        # Print Computation Time if Flag is set
        if self.verbose:
            print("Computation Time for Conversion to Matches (ms)", computation_time_get_matches)
        return matches
    
    # Define a Function to Convert Tracked Corners to Keypoints
    def convert_to_tracked_keypoints(self, corners, curr_corners):
        """
        @brief: Converts Tracked Corners to Matches
        @args:
            param <input> Corners
            param <input> Tracked Corners
        """
        # Convert Tracked Corners to Keypoints
        start_time = time.time()
        kps = [cv2.KeyPoint(corner[0], corner[1], 0) for corner in corners]
        curr_kps = [cv2.KeyPoint(corner[0], corner[1], 0) for corner in curr_corners]
        end_time = time.time()
        computation_time_corner_to_keypoints = 1000 * (end_time - start_time)

        # Print Computation Time if Flag is set
        if self.verbose:
            print("Computation Time for Conversion to Keypoints (ms)", computation_time_corner_to_keypoints)
        return kps, curr_kps
    
    # Define a Function to Apply GMS Filter
    def apply_GMS(self, img, curr_img, kps, curr_kps, matches):
        """
        @brief: Filters Matches based on GMS
        @args:
            param <input> Reference Image
            param <input> Current Image
            param <input> Reference Image Keypoints
            param <input> Current Image Keypoints
            param <input> Matches
        """
        # Apply GMS Filter
        start_time = time.time()
        matches_gms = matchGMS(img.shape[:2], curr_img.shape[:2], 
                           kps, curr_kps, 
                           matches, 
                           withScale = self.scale_flag, 
                           withRotation = self.rotation_flag, 
                           thresholdFactor = self.threshold_factor)
        end_time = time.time()

        # Print Number of Matches Computation Time if Flag is set
        if self.verbose:
            print('Matches after GMS Filter:', len(matches_gms))
            print('Computation Time for GMS Filter (ms)', 1000*(end_time - start_time))
        return matches_gms

    # Define a Function to Retreive Corners from Matches
    def convert_to_tracked_corners(self, matches, kps, curr_kps):
        """
        @brief: Converts to Tracked Corners
        @args:
            param <input> Matches
            param <input> Reference Image Keypoints
            param <input> Current Image Keypoints
        """
        # Initialize Corners
        corners = []
        curr_corners = []

        # Insert for each Match
        for match in matches:
            # Get the Matching Keypoints for each of the Images
            corner_idx = match.queryIdx
            curr_corner_idx = match.trainIdx
            (x1, y1) = kps[corner_idx].pt
            (x2, y2) = curr_kps[curr_corner_idx].pt

            # Append
            corners.append((x1, y1))
            curr_corners.append((x2, y2)) 
        
        return corners, curr_corners

# Define a Class for Pose Estimation
class PoseEstimator:
    # Define Constructor for Pose Estimation
    def __init__(self, verbose = True):
        """
        @brief: Constructor for Pose Estimation Class
        @args:
            param <input> Flag for Verbose
        """
        # Initialize Parameters
        self.verbose = verbose

    # Define a Function to Compute Pose
    def compute_pose(self, corners, curr_corners):
        """
        @brief: Computes Pose
        @args:
            param <input> Corners
            param <input> Tracked Corners
        """
        # Get Transformation Matrix
        start_time = time.time()
        transformation_rigid_matrix, inlier_mask = cv2.estimateAffinePartial2D(curr_corners, 
                                                                          corners, 
                                                                          method=cv2.RANSAC,
                                                                          ransacReprojThreshold=3)
        end_time = time.time()
        computation_time_estimate_transform = 1000 * (end_time - start_time)

        # Print Number of Inliers and Computation Time if Flag is set
        if self.verbose:
            print("Computation Time for Pose Estimation (ms)", computation_time_estimate_transform)
            print("Number of Inliers:", cv2.countNonZero(inlier_mask))
        
        return transformation_rigid_matrix, inlier_mask

    # Define a Function to Compute Stats
    def compute_stats(self, corners, curr_corners, inlier_mask):
        """
        @brief: Computes Stats for Tracked Inliers
        @args:
            param <input> Corners
            param <input> Tracked Corners
            param <input> Inlier Mask
        """
        # Initialize Variables
        distances = []
        num_inliers = cv2.countNonZero(inlier_mask)

        # Compute Stats
        if (num_inliers != 0):
            # Get Distances
            for i in range(len(corners)):
                if (inlier_mask[i][0] == 1):
                    euclidean_distance = cv2.norm(corners[i] - curr_corners[i])
                    distances.append(euclidean_distance)

            # Get Sum and Average Motion Magnitude
            sum = np.sum(distances)
            avg_motion = sum / num_inliers

            # Compute Standard Deviation
            deviation = 0
            for i in range(len(distances)):
                deviation = deviation + np.square(distances[i] - avg_motion)
            std_deviation_motion = np.sqrt(deviation / num_inliers)

        # Print Stats
        if self.verbose:
            print("Average Motion of Inliers:", avg_motion)
            print("Standard Deviation of Inliers:", std_deviation_motion)
        
        return avg_motion, std_deviation_motion, num_inliers

# Define a Class for Feature Detection
class FeatureDetectorDescriptor:
    # Define Constructor for Feature Detector
    def __init__(self, mode = "ORB", output = "Keypoints", verbose = True):
        """
        @brief: Constructor for FeatureDetector Class
        @args:
            param <input> Mode (Currently only ORB is implemented)
            param <input> Output (Currently either Corners or Keypoints or Both)
            param <input> Flag for Verbose
        """
        # Initialize Variables
        self.mode = mode
        self.output = output
        self.verbose = verbose
        
        # Print Parameters if Flag is set
        if self.verbose:
            print("Feature Extractor:", self.mode, "Output:", self.output)
        
        # Set Variables based on Mode 
        if (self.mode == "ORB"):
            # Initiate ORB Feature Detector and Descriptor Object with Default Values
            self.max_num_of_features = 1000
            self.orb = cv2.ORB_create(self.max_num_of_features)
            self.orb.setFastThreshold(0)
        
        # Set Variables for Pixel Location Refinement
        self.win_size = (5,5)
        self.zero_zone = (-1,-1)
        self.criteria = (cv2.TERM_CRITERIA_EPS + cv2.TermCriteria_COUNT,40,0.001)
            
    # Define a Functor to Extract Features
    def extract(self, gray):
        """
        @brief: Extracts Features
        @args:
            param <input> Input Image
        """
        # Extract Features if Mode is Shi-Tomasi
        if (self.mode == "ORB"):
            # Apply Shi-Tomasi Corner Detection
            start_time = time.time()
            keypoints, descriptors = self.orb.detectAndCompute(gray, None)
            end_time = time.time()
            computation_time_feature_detection = 1000 * (end_time - start_time)

            # Print Number of Features and Computation Time if Flag is set
            if self.verbose:
                print("Number of Features", len(keypoints))
                print("Computation Time for Feature Detection and Description (ms)", computation_time_feature_detection)

            # Return
            if (self.output == "Corners"):
                return self.convert_to_corners(keypoints)
            elif (self.output == "Keypoints"):
                return keypoints
    
    # Define to Function to Convert Keypoints to Corners
    def convert_to_corners(self, keypoints):
        """
        @brief: Converts Keypoints to Corners
        @args:
            param <input> Keypoints
        """
        # Convert to Corners
        start_time = time.time()
        corners = cv2.KeyPoint_convert(keypoints)
        end_time = time.time()
        computation_time_keypoints_to_corners = 1000 * (end_time - start_time)

        # Print Computation Time if Flag is set
        if self.verbose:
            print("Computation Time for Conversion from Keypoints to Corners (ms)", computation_time_keypoints_to_corners)
        return corners
    
# Define a Class for Feature Matching
class FeatureMatcher:
    # Define Constructor for Feature Matcher
    def __init__(self, mode = "BF", verbose = True):
        """
        @brief: Constructor for FeatureMatcher Class
        @args:
            param <input> Mode (Currently only Brute-Force Matcher Mode is implemented)
            param <input> Flag for Verbose
        """
        # Initialize Variables
        self.mode = mode
        self.verbose = verbose

        # Set Hyperparameters for GMS Filter
        self.scale_flag = False
        self.rotation_flag = False
        self.threshold_factor = 0

        # Set Matcher based on Mode
        if (self.mode == "BF"):
            # Create Matches Object
            self.matcher = cv2.BFMatcher(cv2.NORM_HAMMING)

    # Define a Function to Match Features
    def match(self, descs, curr_descs):
        """
        @brief: Matches Features
        @args:
            param <input> Input Image
            param <input> Current Image
        """
        # Match Features with Brute-Force
        if (self.mode == "BF"):
            # Compute Matches
            start_time = time.time()
            matches = self.matcher.match(descs, curr_descs)
            end_time = time.time()
            computation_time_matched = 1000 * (end_time - start_time)

            # Print Number of Features Matched Computation Time if Flag is set
            if self.verbose:
                print("Computation Time for Matching (ms)", computation_time_matched)
                print("Number of Features Matches", len(matches))

            return matches

    # Define a Function to Apply GMS Filter
    def apply_GMS(self, img, curr_img, kps, curr_kps, matches):
        """
        @brief: Filters Matches based on GMS
        @args:
            param <input> Reference Image
            param <input> Current Image
            param <input> Reference Image Keypoints
            param <input> Current Image Keypoints
            param <input> Matches
        """
        # Apply GMS Filter
        start_time = time.time()
        matches_gms = matchGMS(img.shape[:2], curr_img.shape[:2], 
                           kps, curr_kps, 
                           matches, 
                           withScale = self.scale_flag, 
                           withRotation = self.rotation_flag, 
                           thresholdFactor = self.threshold_factor)
        end_time = time.time()

        # Print Number of Matches Computation Time if Flag is set
        if self.verbose:
            print('Matches after GMS Filter:', len(matches_gms))
            print('Computation Time for GMS Filter (ms)', 1000*(end_time - start_time))
        return matches_gms
    
    # Define a Function to Retreive Corners from Matches
    def convert_to_tracked_corners(self, matches, kps, curr_kps):
        """
        @brief: Converts to Tracked Corners
        @args:
            param <input> Matches
            param <input> Reference Image Keypoints
            param <input> Current Image Keypoints
        """
        # Initialize Corners
        corners = []
        curr_corners = []

        # Insert for each Match
        for match in matches:
            # Get the Matching Keypoints for each of the Images
            corner_idx = match.queryIdx
            curr_corner_idx = match.trainIdx
            (x1, y1) = kps[corner_idx].pt
            (x2, y2) = curr_kps[curr_corner_idx].pt

            # Append
            corners.append((x1, y1))
            curr_corners.append((x2, y2)) 
        
        return corners, curr_corners