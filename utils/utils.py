# Import Modules
#     __  __    _                    _           _
#   /|  \/  |  (_)                  (_)         (_)
#  | | \  / |   _  __ ____  __   __  _  _ ____   _   _ ____    _____
#  | | |\/| | /| |/ /  _  \/\ \ / //| |/ / __/ /| | / / _  \ /| '_  \
#  | | |  | || | || | (_) |\ \ V /| | |\ \__ \| | || | (_) || | | | |
#  |_|_|  |_||_|_/ \_\___/  \_\_/ |_|_/|_|___/|_|_/ \_\___/ |_|_/ |_/
#
#  Copyright 2022, Miovision
#  @name    util.py 
#  @brief   The file contains Util Functions for Pose Estimation
#  @author  Umer Rasheed

import cv2
import numpy as np

# Define a Function to Preprocess Input Frame
def preprocess_frame(frame, image_width, image_height):
    """
    @brief: Convert BGR Image to Resized RGB and Grayscale Images
    @args:
        param <input> Input Frame
        param <input> Image Width
        param <input> Image Height
    """
    # Resize Reference Image
    img = cv2.resize(frame, (image_width, image_height), interpolation=cv2.INTER_AREA)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    
    # Convert to Grayscale Image
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    
    return img, gray
    
# Define a Function to Load Video from Path Check Video Properties
def load_video(path):
    """
    @brief: Load Video from Input Path and Returns Video Objec and Properties
    @args:
        param <input> Input Path
    """
    video = cv2.VideoCapture(path)
    fps = video.get(cv2.CAP_PROP_FPS)
    total_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    return video, fps, total_frames

# Define a Function to Draw Matches
def draw_matches(src_a, src_b, kp_a, kp_b, matches, color=np.random.randint(0,255,(500,3))):
    """
    @brief: Draws Matches on Output Images
    @args:
        src_a <input> First Image
        src_b <input> Second Image
        kp_a <input> Keypoints from First Image
        kp_b <input> Keypoints from Second Image
        matches <input> Keypoint Matches
        color <input> Color Mode
    """
    # Get Height and Width
    height = max(src_a.shape[0], src_b.shape[0])
    width = src_a.shape[1] + src_b.shape[1]
    
    # Initialize
    output_img = np.zeros((height, width, 3), dtype=np.uint8)
    output_img[0:src_a.shape[0], 0:src_a.shape[1]] = src_a
    output_img[0:src_b.shape[0], src_a.shape[1]:] = src_b[:]

    # Add Matches
    for i in range(len(matches)):
        left_kp = kp_a[matches[i].queryIdx].pt
        right_kp = tuple(sum(x) for x in zip(kp_b[matches[i].trainIdx].pt, (src_a.shape[1], 0)))
        cv2.line(output_img, tuple(map(int, left_kp)), tuple(map(int, right_kp)), color[i].tolist())

    return output_img


# Define a Function to Check Reliability of Transform
def is_transform_reliable(num_inliers, std_deviation_motion, 
                          min_inliers = 25,
                          max_std_deviation_motion = 10):
    """
    @brief: Checks if Transform is Reliable
    @args:
        num_inliers <input> Number of Inliers
        std_deviation_motion <input> Standard Deviation of Inlier's Motion
        min_inliers <input> Minimum Inliers
        max_std_deviation_motion <input> Maximum Standard Deviation of Inlier's Motion
    """
    is_transform_reliable = (num_inliers > min_inliers and std_deviation_motion < max_std_deviation_motion)
    print("Transform Quality", is_transform_reliable)
    return is_transform_reliable